package section.login;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import section.login.dto.AppRoleDto;
import section.login.dto.AppUserDto;
import section.login.entities.AppRole;
import section.login.entities.AppUser;
import section.login.service.AppServiceImpl;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class LoginApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginApplication.class, args);
    }

    @Bean
    CommandLineRunner start(AppServiceImpl appService){
        return args -> {
            List<AppRole> roles = new ArrayList<>();
            AppRole r = appService.addR(new AppRoleDto("ADMIN"));
            roles.add(r);
            r = appService.addR(new AppRoleDto("USER"));
            roles.add(r);
            AppUser user = appService.add(new AppUserDto("Yara","Baba","baba2018","passer2018","baba@gmail.com",true));
            appService.addRoleToUser(user, roles);

        };
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }
}
