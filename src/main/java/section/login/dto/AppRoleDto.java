package section.login.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AppRoleDto {
    private int id;
    private String libelle;

    public AppRoleDto(String libelle) {
        this.libelle = libelle;
    }
}
