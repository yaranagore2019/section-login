package section.login.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AppUserDto {
    private int id;
    private String nom;
    private String prenom;
    private String username;
    private String password;
    private String email;
    private boolean actived;
    private Collection<AppRoleDto> appRoles = new ArrayList<>();

    public AppUserDto(String nom, String prenom, String username, String password, String email, boolean actived) {
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.password = password;
        this.email = email;
        this.actived = actived;
    }
}
