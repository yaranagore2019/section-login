package section.login.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String libelle;
    @ManyToMany(mappedBy = "appRoles")
    private Collection<AppUser> appUsers;

    @Override
    public String toString() {
        return "AppRole{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                '}';
    }
}
