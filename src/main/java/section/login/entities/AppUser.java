package section.login.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nom;
    private String prenom;
    private String username;
    private String password;
    private boolean actived;
    private String email;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "app_user_role"
            , joinColumns = {
            @JoinColumn(name = "appuser_id")
    }
            , inverseJoinColumns = {
            @JoinColumn(name = "approle_id")
    }
    )
    private Collection<AppRole> appRoles = new ArrayList<>();
}
