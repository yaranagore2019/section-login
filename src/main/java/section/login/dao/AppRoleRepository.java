package section.login.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import section.login.entities.AppRole;


@RepositoryRestResource
public interface AppRoleRepository extends JpaRepository<AppRole, Integer> {
    AppRole findByLibelle(String roleName);
}
