package section.login.service;

import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import section.login.dao.AppRoleRepository;
import section.login.dao.AppUserRepository;
import section.login.dto.AppRoleDto;
import section.login.dto.AppUserDto;
import section.login.entities.AppRole;
import section.login.entities.AppUser;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AppServiceImpl implements AppService, UserDetailsService {

    private AppUserRepository appUserRepository;

    private AppRoleRepository appRoleRepository;

    private BCryptPasswordEncoder encoder;

    public AppServiceImpl(AppUserRepository appUserRepository, AppRoleRepository appRoleRepository, BCryptPasswordEncoder encoder) {
        this.appUserRepository = appUserRepository;
        this.appRoleRepository = appRoleRepository;
        this.encoder = encoder;
    }

    @Override
    public AppUser add(AppUserDto appUserDto) {
        if (appUserDto != null && findOne(appUserDto.getUsername()) == null) {
            AppUser user = new AppUser();
            user.setActived(true);
            user.setEmail(appUserDto.getEmail());
            user.setNom(appUserDto.getNom());
            user.setPassword(encoder.encode(appUserDto.getPassword()));
            user.setUsername(appUserDto.getUsername());
            user.setPrenom(appUserDto.getPrenom());
            List<AppRole> userRoles = new ArrayList<>();
            if (appUserDto.getAppRoles() != null) {
                for (AppRoleDto dto : appUserDto.getAppRoles()) {
                    AppRole r = appRoleRepository.findByLibelle(dto.getLibelle());
                    if (r != null) {
                        userRoles.add(r);
                    }
                }
                user.setAppRoles(userRoles);
            }
            return appUserRepository.save(user);
        }
        return null;
    }

    @Override
    public List<AppUser> all() {
        List<AppUser> all = new ArrayList<>();
        appUserRepository.findAll().iterator().forEachRemaining(all::add);
        return all;
    }

    @Override
    public AppUser getOne(int id) {
        Optional<AppUser> optional = appUserRepository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public void delete(int id) {
        appUserRepository.deleteById(id);
    }

    @Override
    public AppUser findOne(String username) {
        return appUserRepository.findByUsername(username);
    }

    @Override
    public AppUserDto update(AppUserDto appUserDto) {
        return null;
    }

    @Override
    public AppRole addR(AppRoleDto appRoleDto) {
        if (appRoleDto != null && findOneR(appRoleDto.getLibelle()) == null) {
            AppRole role = new AppRole();
            role.setLibelle(appRoleDto.getLibelle());
            return appRoleRepository.save(role);
        }
        return null;
    }

    @Override
    public List<AppRole> allR() {
        List<AppRole> all = new ArrayList<>();
        appRoleRepository.findAll().iterator().forEachRemaining(all::add);
        return all;
    }

    @Override
    public AppRole getOneR(int id) {
        Optional<AppRole> optional = appRoleRepository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public void deleteR(int id) {
        appRoleRepository.deleteById(id);
    }

    @Override
    public AppRole findOneR(String roleName) {
        return appRoleRepository.findByLibelle(roleName);
    }

    @Override
    public AppRoleDto updateR(AppRoleDto appRoleDto) {
        return null;
    }

    @Override
    public boolean addRoleToUser(AppUser appUser, List<AppRole> appRoles) {
        if (appRoles != null){
            appUser.setAppRoles(appRoles);
            appUserRepository.save(appUser);
            return true;
        }
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        AppUser user = findOne(s);
        if (user == null){
            throw new UsernameNotFoundException("Utilisateur n'existe pas");
        }
        else {
            Collection<GrantedAuthority> authorities = new ArrayList<>();
            user.getAppRoles().forEach(r ->{
                authorities.add(new SimpleGrantedAuthority(r.getLibelle()));
            });
            return new User(user.getUsername(), user.getPassword(), authorities);
        }
    }
}
