package section.login.service;

import section.login.dto.AppRoleDto;
import section.login.dto.AppUserDto;
import section.login.entities.AppRole;
import section.login.entities.AppUser;

import java.util.List;

public interface AppService {
    AppUser add(AppUserDto appUserDto);
    List<AppUser> all();
    AppUser getOne(int id);
    void delete(int id);
    AppUser findOne(String username);
    AppUserDto update(AppUserDto appUserDto);
    AppRole addR(AppRoleDto appRoleDto);
    List<AppRole> allR();
    AppRole getOneR(int id);
    void deleteR(int id);
    AppRole findOneR(String roleName);
    AppRoleDto updateR(AppRoleDto appRoleDto);

    boolean addRoleToUser(AppUser appUser, List<AppRole> appRoles);
}
